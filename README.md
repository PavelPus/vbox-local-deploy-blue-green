Задание 7.4

git clone https://gitlab.com/PavelPus/vbox-local-deploy-blue-green.git

cd vbox-local-deploy-blue-green

Меняем в hosts ip-адрес на адрес гостевой машины VBox

Меняем в файлах ansible_sudo_pass

Запуск:

ansible-playbook -b local-reactjs.yaml -vv

ansible-playbook -i hosts -b remote-reactjs.yaml -vv

Маппим папку в гостевую машину на путь:

/var/www/releases/local

Заходим в гостевую машину, переходим в /var/www/releases/local

Запускаем 

yarn start
